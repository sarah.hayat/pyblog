from django.db import models
from django.template.defaultfilters import slugify


class PostCategory(models.Model):
    name = models.CharField(max_length=50)
    
    def slug(self):
        return slugify(self.name)
    
    def __str__(self):
        return '{}'.format(self.name)


class Post(models.Model):
    category = models.ForeignKey('PostCategory', on_delete=models.DO_NOTHING, null=True, blank=True)
    title = models.CharField(max_length=150)
    published = models.BooleanField(default=False)
    text = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return '{}'.format(self.title)


class Comment(models.Model):
    STATUS_VISIBLE = 'visible'
    STATUS_HIDDEN = 'hidden'
    STATUS_MODERATE = 'moderate'
    
    STATUS_CHOICES = (
        (STATUS_VISIBLE, 'Visible'),
        (STATUS_HIDDEN, 'Hidden'),
        (STATUS_MODERATE, 'Moderate'),
    )
    
    post = models.ForeignKey('Post', on_delete=models.CASCADE, related_name='comments')
    author_name = models.CharField(max_length=100)
    text = models.TextField()
    status = models.CharField(max_length=20, default=STATUS_VISIBLE, choices=STATUS_CHOICES)
    moderation_text = models.CharField(max_length=200, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return '{} - {}, status =({})'.format(self.author_name, self.text[:20], self.status)
